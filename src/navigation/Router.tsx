import React from 'react';
import {LinkingOptions, NavigationContainer} from '@react-navigation/native';
import { Image } from 'react-native';
import {createNativeStackNavigator} from '@react-navigation/native-stack';
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';

import {SafeAreaProvider} from 'react-native-safe-area-context';
import { colors } from '../styles';

import HomeScreen from '../screens/Home';
import Points from '../screens/Points';
import SplashScreen from '../screens/Splash';
import Common from '../utilities/Common';
import HeaderTitle from '../components/HeaderTitle';
import MapList from '../components/buttons/MapList';

export const Stack = createNativeStackNavigator();
const Tab = createBottomTabNavigator();

const linking: LinkingOptions = {
  prefixes: ['ufawiki://'],
  config: {
    screens: {},
  },
};

const ObjectTab = () => {
  return (
      <Stack.Navigator>
          <Stack.Screen name={'Points'} component={Points} options={{
            headerTransparent: true,
            headerTitle: (props) => <HeaderTitle {...props} title={'UFA.WIKI'} />,
            headerRight: (props) => (
              <MapList
                {...props}
              />
            ),
              // animation: 'slide_from_bottom',
              // presentation: 'card',
          }}/>
      </Stack.Navigator>
  );
}

const ARTab = () => {
  return (
      <Stack.Navigator>
          <Stack.Screen name={'ARScreen'} component={HomeScreen} options={{
            headerTransparent: true,
            headerTitle: (props) => <HeaderTitle {...props} title={'UFA.WIKI'} />
              // animation: 'slide_from_bottom',
              // presentation: 'card',
          }}/>
      </Stack.Navigator>
  );
}

const Tabs = () => {
    return (
        <Tab.Navigator screenOptions={({ route }) => ({
            tabBarStyle: {
              backgroundColor: colors.TABBAR_COLOR,
              // borderTopLeftRadius: 20,
              // borderTopRightRadius: 20,
              borderTopWidth: 0,
              borderRadius: 50,
              position: 'absolute',
              bottom: 0,
              shadowColor: '#CED6E8',
              shadowOffset: { width: 0, height: 2 },
              shadowOpacity: 1,
              shadowRadius: 10,
              elevation: 4,
            },
            tabBarIcon: ({ focused, color, size }) => {
              let iconName;
  
              if (route.name === 'Objects') {
                iconName = require('./../assets/ic-tab-points.png');
              } else if (route.name === 'AR') {
                iconName = require('./../assets/ic-tab-ar.png');
              } else if (route.name === 'Photo') {
                iconName = require('./../assets/ic-tab-photo.png');
              } else if (route.name === 'Profile') {
                iconName = require('./../assets/ic-tab-profile.png');
              }
  
              // You can return any component that you like here!
              return (<Image
                source={iconName}
                style={{
                  marginTop: 10,
                  resizeMode: 'contain',
                  width: 24,
                  height: 24,
                  tintColor: color,
                }}
              />);
            },
            tabBarActiveTintColor: '#696FDF',
            tabBarInactiveTintColor: 'gray',
            tabBarLabelStyle: {
              marginBottom: -5,
              fontSize: 12,
              fontFamily: 'OpenSans-Regular',
            }
            
          })}>
            <Tab.Screen name={'Objects'} component={ObjectTab} options={{
                title: 'Объекты',
                headerShown: false,
            }}/>
            <Tab.Screen name={'AR'} component={ARTab} options={{
                title: 'AR',
                headerShown: false,
                // animation: 'slide_from_bottom',
                // presentation: 'card',
            }}/>
            <Tab.Screen name={'Photo'} component={HomeScreen} options={{
                title: 'Фото',
                // animation: 'slide_from_bottom',
                // presentation: 'card',
            }}/>
            <Tab.Screen name={'Profile'} component={HomeScreen} options={{
                title: 'Профиль',
                // animation: 'slide_from_bottom',
                // presentation: 'card',
            }}/>
        </Tab.Navigator>
    );
}

export default function Router() {

    return (
        <NavigationContainer>
          <SafeAreaProvider>
          <Stack.Navigator>
                <Stack.Screen
                    name={'Splash'}
                    component={SplashScreen}
                    options={{
                        headerShown: false,
                    }}
                />
                <Stack.Screen name={'Login'} component={Tabs} options={{
                    animation: 'slide_from_bottom',
                    presentation: 'card',
                    headerShown: false,
                    gestureEnabled: false,
                }}/>
            </Stack.Navigator>
          </SafeAreaProvider>
        </NavigationContainer>
      );

//   return (
//     <NavigationContainer>
//       <SafeAreaProvider>
//       <Stack.Navigator>
//             <Stack.Group>
//                 <Stack.Screen
//                     name={'Splash'}
//                     component={SplashScreen}
//                     options={{
//                         headerShown: false,
//                     }}
//                 />
//             </Stack.Group>
//             <Stack.Group screenOptions={{ presentation: 'modal' }}>
//                 <Stack.Screen name={'Home'} component={HomeScreen} />
//             </Stack.Group>
//         </Stack.Navigator>
//       </SafeAreaProvider>
//     </NavigationContainer>
//   );
}
