import { Models } from "@rematch/core";
import points from './points';
import buttons from './buttons';
import categories from './categories';
import location from './location';

export interface RootModel extends Models<RootModel> {
	points: typeof points;
	buttons: typeof buttons;
	categories: typeof categories;
	location: typeof location;
}

export const models: RootModel = { points, buttons, categories, location };