import axios from 'axios';
import { BASE_URL } from './../../../constants';

class CommonAPI {
	getPoints = () => {
		return axios.get(BASE_URL + '/api/user/get-points');
	};

	getCategories = () => {
		return axios.get(BASE_URL + '/api/user/get-categories');
	};
}

const commonRequests = new CommonAPI();
export default commonRequests;
