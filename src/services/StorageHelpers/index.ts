import AsyncStorage from '@react-native-async-storage/async-storage';

class StorageService {
	saveToken = (token: string) => {
		return Promise.all([AsyncStorage.setItem('token', token)]);
	};

	getToken = () => {
		return AsyncStorage.getItem('token');
	};

	removeAll = () => {
		return AsyncStorage.clear();
	};
}
const StorageHelper = new StorageService();
export default StorageHelper;
