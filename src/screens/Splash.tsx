import React, { useCallback, useEffect, useRef } from 'react';
import { StyleSheet, StatusBar, View } from 'react-native';
import { SafeAreaView } from 'react-native-safe-area-context';

import { theme } from './../../theme';

import { StackNavigationProp } from '@react-navigation/stack';
import { useNavigation } from '@react-navigation/native';

const SplashScreen = ({navigation}) => {
  const isMount = useRef<boolean>(false);

  useEffect(() => {
    setTimeout(() => {
        navigation.navigate('Login');
    }, 3000);
  }, []);

  return (
        <View style={{
            flex: 1,
            backgroundColor: 'green',
        }}>
          </View>
  );
};
export default SplashScreen;

const styles = StyleSheet.create({
  safeArea: {
    flex: 1,
    backgroundColor: theme.colors.backgroundColor,
  },
});
