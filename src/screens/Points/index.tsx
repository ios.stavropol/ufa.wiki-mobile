import React, { useEffect } from 'react';
import {StyleSheet, FlatList, StatusBar, View, RefreshControl, Text, TouchableOpacity, Image} from 'react-native';
import { useRoute } from '@react-navigation/native';
import { RootState, Dispatch } from './../../store';
import { connect, useDispatch, useSelector } from 'react-redux';
import Common from '../../utilities/Common';
import PointView from '../../components/Points/PointView';
import ListView from '../../components/Points/ListView';
import MapView2 from '../../components/Points/MapView2';
import { useHeaderHeight } from '@react-navigation/elements';
import BackgroundGeolocation from "react-native-background-geolocation";

const Points = ({setPoints, getCategories, getPoints, isLoading, points, pointsRaw, maplist, setCoords}) => {
	const params = useRoute().params;
    const headerHeight = useHeaderHeight();
    // const dispatch = useDispatch<Dispatch>()
    // const pointsState = useSelector((state: RootState) => state.points)

	//@ts-ignore
	const data = params ? params.data : null;
	console.log('Points -> data', data);

	const [refreshing, setRefreshing] = React.useState(false);

    useEffect(() => {
        // This handler fires whenever bgGeo receives a location update.
        BackgroundGeolocation.onLocation(onLocation, onError);

        // // This handler fires when movement states changes (stationary->moving; moving->stationary)
        // BackgroundGeolocation.onMotionChange(onMotionChange);

        // // This event fires when a change in motion activity is detected
        // BackgroundGeolocation.onActivityChange(onActivityChange);

        // This event fires when the user toggles location-services authorization
        BackgroundGeolocation.onProviderChange(onProviderChange);

        ////
        // 2.  Execute #ready method (required)
        //
        BackgroundGeolocation.ready({
            // Geolocation Config
            desiredAccuracy: BackgroundGeolocation.DESIRED_ACCURACY_HIGH,
            distanceFilter: 10,
            // Activity Recognition
            stopTimeout: 1,
            // Application config
            // debug: true, // <-- enable this hear sounds for background-geolocation life-cycle.
            // logLevel: BackgroundGeolocation.LOG_LEVEL_VERBOSE,
            stopOnTerminate: false,   // <-- Allow the background-service to continue tracking when user closes the app.
            startOnBoot: true,        // <-- Auto start tracking when device is powered-up.
            // HTTP / SQLite config
            // url: 'http://yourserver.com/locations',
            // batchSync: false,       // <-- [Default: false] Set true to sync locations to server in a single HTTP request.
            // autoSync: true,         // <-- [Default: true] Set true to sync each location to server as it arrives.
            // headers: {              // <-- Optional HTTP headers
            //     "X-FOO": "bar"
            // },
            // params: {               // <-- Optional HTTP params
            //     "auth_token": "maybe_your_server_authenticates_via_token_YES?"
            // }
            }, (state) => {
                console.warn("- BackgroundGeolocation is configured and ready: ", state.enabled);

                if (!state.enabled) {
                    BackgroundGeolocation.start(function() {
                        console.warn("- Start success");
                    });
                }
        });
    });

    const onLocation = location => {
        console.warn('[location] -', location);
        setCoords(location.coords);
    }

    const onError = error => {
        console.warn('[location] ERROR -', error);
    }

    // const onActivityChange = event => {
    //     console.warn('[activitychange] -', event);  // eg: 'on_foot', 'still', 'in_vehicle'
    // }

    const onProviderChange = provider => {
        console.warn('[providerchange] -', provider.enabled, provider.status);
    }

    // const onMotionChange = event => {
    //     console.warn('[motionchange] -', event.isMoving, event.location);
    // }

    useEffect(() => {
        onRefresh();
    }, []);

    const onRefresh = () => {
        getPoints();
        getCategories();
    }

    const renderRow = (item: object, index: number) => {
        return (<PointView
            item={item}
            index={index}
            last={points.points.length === index + 1 ? true : false}
            onPress={() => {

            }}
        />);
    }

	return (
		<View style={{
            flex: 1,
            backgroundColor: 'white',
            marginBottom: -20,
        }}>
            <StatusBar barStyle="light-content" backgroundColor="#6a51ae" />
            <Image
                source={require('./../../assets/ic-top.png')}
                style={{
                    position: 'absolute',
                    top: 0,
                    resizeMode: 'cover',
                    width: Common.getLengthByIPhone7(0),
                    height: Common.getLengthByIPhone7(230),
                }}
            />
            {maplist === 'list' ? (<ListView
                onRefresh={() => {
                    onRefresh();
                }}
                isLoading={isLoading}
                points={points}
            />) : (<MapView2
                isLoading={isLoading}
                points={points}
            />)}
        </View>
	);
};
const mstp = (state: RootState) => ({
	isLoading: state.points.isRequestGoing,
	points: state.points.points,
    pointsRaw: state.points.pointsRaw,
    maplist: state.buttons.maplist,
});

const mdtp = (dispatch: Dispatch) => ({
    getFavorites: () => dispatch.buttons.getFavorites(),
    setPoints: payload => dispatch.points.setPoints(payload),
	getPoints: () => dispatch.points.getPoints(),
    getCategories: () => dispatch.categories.getCategories(),
    setRequestGoingStatus: payload => dispatch.points.setRequestGoingStatus(payload),
    setCoords: payload => dispatch.location.setCoords(payload),
});

export default connect(mstp, mdtp)(Points);

const styles = StyleSheet.create({
  safeArea: {
    flex: 1,
    backgroundColor: 'white',
  },
});
