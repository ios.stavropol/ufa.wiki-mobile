import HomeScreen from './Home';
// import CustomWebView from './CharacterDetail';
/* PLOP_INJECT_IMPORT */
import { StackNavigationOptions } from '@react-navigation/stack';

const options: StackNavigationOptions = { gestureEnabled: false };

export const commonScreens = {
  Home: { component: HomeScreen, options },
//   CharacterDetail: { component: CustomWebView },
  /* PLOP_INJECT_SCREEN */
};
