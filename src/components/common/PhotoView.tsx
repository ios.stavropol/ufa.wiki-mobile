import React, { useEffect } from 'react';
import {View, Image} from 'react-native';
import Common from '../../utilities/Common';
import FastImage from 'react-native-fast-image';
import LinearGradient from 'react-native-linear-gradient';
import { createShimmerPlaceholder } from 'react-native-shimmer-placeholder'

const ShimmerPlaceHolder = createShimmerPlaceholder(LinearGradient)

const PhotoView = ({source, style}) => {
	
	const [stage, setStage] = React.useState(0);
	const [placeholder, setPlaceholder] = React.useState(null);
	
	useEffect(() => {
        if (stage === 0) {
			setPlaceholder(<ShimmerPlaceHolder
				style={[{
					width: Common.getLengthByIPhone7(0) - Common.getLengthByIPhone7(32),
					height: Common.getLengthByIPhone7(150),
					position: 'absolute',
					left: 0,
					top: 0,
					zIndex: 200,
				}, style]}
			/>);
		} else if (stage === 1) {
			setPlaceholder(null);
		} else {
			setPlaceholder(<Image
				style={[{
					resizeMode: 'cover',
					width: Common.getLengthByIPhone7(0) - Common.getLengthByIPhone7(32),
					height: Common.getLengthByIPhone7(150),
					position: 'absolute',
					left: 0,
					top: 0,
					zIndex: 100,
				}, style]}
				source={require('./../../assets/ic-placeholder-monument.png')}
			/>);
		}
    }, [stage]);

	return (
	<View style={{

	}}>
		<FastImage
			source={{
				uri: source,
				priority: FastImage.priority.normal,
			}}
			style={[{
				resizeMode: 'cover',
				width: Common.getLengthByIPhone7(0) - Common.getLengthByIPhone7(32),
				height: Common.getLengthByIPhone7(150),
			}, style]}
			onLoadStart={() => {
				setStage(0);
			}}
			onLoad={() => {
				setStage(1);
			}}
			onError={() => {
				setStage(2);
			}}
		/>
		{placeholder}
	</View>
	);
};

export default PhotoView;