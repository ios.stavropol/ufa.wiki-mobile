import React, { useEffect } from 'react';
import {View, Text} from 'react-native';
import Common from './../utilities/Common';

interface PropTypes {
	title: string,
}

const HeaderTitle = ({title}: PropTypes) => {
	
	return (
		<View style={{
			width: Common.getLengthByIPhone7(0) - Common.getLengthByIPhone7(32),
			backgroundColor: 'transparent',
			alignItems: 'flex-start',
		}}>
			<Text style={{
				width: Common.getLengthByIPhone7(0) - Common.getLengthByIPhone7(52),
				color: 'white',
				fontFamily: 'Rubik-Medium',
				fontWeight: '500',
				textAlign: 'left',
				fontSize: Common.getLengthByIPhone7(34),
				lineHeight: Common.getLengthByIPhone7(40),
				letterSpacing: -0.33,
			}}
			allowFontScaling={false}>
				{title}
			</Text>
		</View>
	);
};

export default HeaderTitle;