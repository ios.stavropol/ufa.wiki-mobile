import React, { useEffect } from 'react';
import {StyleSheet, TextInput, View, RefreshControl, Text, TouchableOpacity} from 'react-native';
import { useRoute } from '@react-navigation/native';
import { RootState, Dispatch } from './../../store';
import { connect, useDispatch, useSelector } from 'react-redux';
import Common from '../../utilities/Common';
import { colors } from '../../styles';

const SearchView = ({setPoints, pointsRaw}) => {
	const params = useRoute().params;

	const [search, setSearch] = React.useState('');

	useEffect(() => {
		if (search.length) {
			let newArray = pointsRaw.filter(el => {
				return el.name.toLowerCase().indexOf(search.toLowerCase()) !== -1;
			});
			setPoints({points: newArray});
		} else {
			setPoints({points: pointsRaw});
		}
	}, [search])

    return (
		<View style={{
			width: Common.getLengthByIPhone7(0),
			alignItems: 'center',
		}}>
			<TextInput
				style={{
					width: Common.getLengthByIPhone7(0) - Common.getLengthByIPhone7(32),
					height: Common.getLengthByIPhone7(52),
					borderRadius: Common.getLengthByIPhone7(16),
					backgroundColor: '#f8f8f8',
					paddingLeft: Common.getLengthByIPhone7(16),
					shadowColor: "#000",
					shadowOffset: {
						width: 0,
						height: 1,
					},
					shadowOpacity: 0.18,
					shadowRadius: 5.00,
					elevation: 1,
				}}
				clearButtonMode={'while-editing'}
				allowFontScaling={false}
				contextMenuHidden={false}
				spellCheck={true}
				autoCorrect={false}
				placeholder={'Поиск...'}
				// placeholderTextColor={this.props.placeholderTextColor}
				autoCompleteType={'off'}
				// inputAccessoryViewID={this.props.inputAccessoryViewID}
				multiline={false}
				numberOfLines={1}
				returnKeyType={'search'}
				secureTextEntry={false}
				autoCapitalize={'none'}
				underlineColorAndroid={'transparent'}
				onSubmitEditing={() => {
					
				}}
				// ref={el => this.textInputRef = el}
				onFocus={() => {
					
				}}
				onBlur={() => {
					
				}}
				onChangeText={(text) => {
					setSearch(text);
				}}
				value={search}
			/>
		</View>
	);
};

const mstp = (state: RootState) => ({
	pointsRaw: state.points.pointsRaw,
});

const mdtp = (dispatch: Dispatch) => ({
	setPoints: payload => dispatch.points.setPoints(payload),
});

export default connect(mstp, mdtp)(SearchView);