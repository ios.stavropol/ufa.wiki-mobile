import React, { useEffect } from 'react';
import {StyleSheet, FlatList, TextInput, View, RefreshControl, Text, TouchableOpacity, Image} from 'react-native';
import { useRoute } from '@react-navigation/native';
import { RootState, Dispatch } from './../../store';
import Common from '../../utilities/Common';
import MapView, { PROVIDER_GOOGLE, Marker } from 'react-native-maps';
import { useHeaderHeight } from '@react-navigation/elements';
import TagsView from './TagsView';
import SearchView from './SearchView';

const MapView2 = ({points, isLoading}) => {
	const params = useRoute().params;
    const headerHeight = useHeaderHeight();
    
	const [search, setSearch] = React.useState('');

    useEffect(() => {
        
    }, []);

	return (
		<View style={{
            flex: 1,
            backgroundColor: 'transparent',
            marginBottom: -20,
            marginTop: headerHeight + 20,
        }}>
			<MapView
				provider={PROVIDER_GOOGLE}
				style={{
					width: Common.getLengthByIPhone7(0),
					flex: 1,
					marginTop: -10,
				}}
				region={{
					latitude: 54.7430600,
					longitude: 55.9677900,
					latitudeDelta: 0.15,
					longitudeDelta: 0.121,
				}}
			>
				{points.points.map((marker, index) => (
					<Marker
						key={index}
						coordinate={{latitude: marker.latitude, longitude: marker.longitude}}
						title={marker.name}
						description={marker.description}
					/>
				))}
			</MapView>
			<View style={{
				width: Common.getLengthByIPhone7(0),
				position: 'absolute',
				left: 0,
				top: 0,
			}}>
				<SearchView/>
				<TagsView/>
			</View>
        </View>
	);
};

export default MapView2;