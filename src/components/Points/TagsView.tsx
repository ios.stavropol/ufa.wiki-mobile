import React, { useEffect } from 'react';
import {StyleSheet, ScrollView, View, RefreshControl, Text, TouchableOpacity} from 'react-native';
import { useRoute } from '@react-navigation/native';
import { RootState, Dispatch } from './../../store';
import { connect, useDispatch, useSelector } from 'react-redux';
import Common from '../../utilities/Common';
import { colors } from '../../styles';

const TagsView = ({setPoints, pointsRaw, categories, selectedCategory, setCategories, setSelectedCategory}) => {
	const params = useRoute().params;
    // const dispatch = useDispatch<Dispatch>()
    // const pointsState = useSelector((state: RootState) => state.points)

	const [tags, setTags] = React.useState([]);

	useEffect(() => {
		let array = [];
		for (let i = 0; i < categories.length; i++) {
			array.push(renderTag(categories[i].name, categories[i].id, {
				marginRight: i + 1 === categories.length ? Common.getLengthByIPhone7(16) : 0,
			}));
		}
		setTags(array);

		if (selectedCategory.includes(0)) {
			setPoints({points: pointsRaw});
		} else if (selectedCategory.includes(-1)) {
			setPoints({points: []});
		} else {
			if (pointsRaw) {
				console.warn('pointsRaw: '+JSON.stringify(pointsRaw));
				let newArray = pointsRaw.filter(el => {
					if (el.cats) {
						console.warn(el.cats);
						for (let i = 0; i < el.cats.length; i++) {
							if (selectedCategory.includes(el.cats[i].id)) {
								return el;
							}
						}
					}
				});
				setPoints({points: newArray});
			}
		}
	}, [selectedCategory, categories])

	const renderTag = (title: string, id: number, style) => {
		let isSelected = selectedCategory.includes(id);
		return(<TouchableOpacity style={[{
			paddingLeft: Common.getLengthByIPhone7(16),
			paddingRight: Common.getLengthByIPhone7(16),
			// width: Common.getLengthByIPhone7(150),
			height: Common.getLengthByIPhone7(52),
			borderRadius: Common.getLengthByIPhone7(26),
			marginLeft: Common.getLengthByIPhone7(16),
			backgroundColor: isSelected ? colors.MAIN_COLOR : 'white',
			borderWidth: 1,
			borderColor: colors.MAIN_COLOR,
			shadowColor: "#000",
			shadowOffset: {
				width: 0,
				height: 1,
			},
			shadowOpacity: 0.18,
			shadowRadius: 5.00,
			elevation: 1,
			alignItems: 'center',
			justifyContent: 'center',
		}, style]}
		onPress={() => {
			if (id === 0 || id === -1) {
				setSelectedCategory([id]);
			} else {
				let array = JSON.parse(JSON.stringify(selectedCategory));
				if (array.includes(id)) {
					for(let i = 0; i < array.length; i++) {
						if ( array[i] === id) { 
							array.splice(i, 1);
							break;
						}
					}
				} else {
					array.push(id);
				}
				for(let i = 0; i < array.length; i++) {
					if (array[i] === 0 || array[i] === -1) { 
						array.splice(i, 1);
						// break;
					}
				}
				console.warn(array);
				if (array.length === 0) {
					array.push(0);
				}
				setSelectedCategory(array);
			}
		}}
		key={id}>
			<Text style={{
				color: isSelected ? 'white' : colors.MAIN_COLOR,
				fontFamily: 'OpenSans-SemiBold',
				fontWeight: '600',
				textAlign: 'left',
				fontSize: Common.getLengthByIPhone7(16),
				lineHeight: Common.getLengthByIPhone7(18),
				letterSpacing: -0.33,
			}}
			allowFontScaling={false}>
				{title}
			</Text>
		</TouchableOpacity>);
	}

    return (
		<View style={{
            backgroundColor: 'transparent',
			marginTop: 15,
        }}>
            <ScrollView style={{
				width: Common.getLengthByIPhone7(0),
				height: Common.getLengthByIPhone7(60),
			}}
			contentContainerStyle={{
				alignItems: 'center',
			}}
			horizontal={true}
			pagingEnabled={false}
			showsHorizontalScrollIndicator={false}
			scrollEventThrottle={16}
			// ref={el => this.scroll = el}
			bounces={false}>
				{tags}
			</ScrollView>
        </View>
	);
};

const mstp = (state: RootState) => ({
	categories: state.categories.categories,
	selectedCategory: state.categories.selectedCategory,
	pointsRaw: state.points.pointsRaw,
});

const mdtp = (dispatch: Dispatch) => ({
	setPoints: payload => dispatch.points.setPoints(payload),
	setCategories: payload => dispatch.categories.setCategories(payload),
    setSelectedCategory: payload => dispatch.categories.setSelectedCategory(payload),
});

export default connect(mstp, mdtp)(TagsView);