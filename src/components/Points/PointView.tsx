import React, { useEffect } from 'react';
import {View, Text, TouchableOpacity, Image} from 'react-native';
import Common from '../../utilities/Common';
import {BASE_URL} from './../../constants';
import { colors } from '../../styles';
import PhotoView from './../common/PhotoView';
import { RootState, Dispatch } from './../../store';
import { connect, useDispatch, useSelector } from 'react-redux';
import { getDistance } from 'geolib';

const PointView = ({onPress, item, like, favorites, last, coords}) => {
	
	const [distance, setDistance] = React.useState(0);

	useEffect(() => {
		if (coords) {
			setDistance(Math.round(getDistance(coords, {latitude: item.latitude, longitude: item.longitude})/10) / 100);
		} else {
			setDistance(0);
		}
	}, [coords]);

	useEffect(() => {
		
	}, [favorites]);

	return (
		<TouchableOpacity style={{
			width: Common.getLengthByIPhone7(0) - Common.getLengthByIPhone7(32),
			borderRadius: Common.getLengthByIPhone7(10),
			marginTop: Common.getLengthByIPhone7(16),
			marginBottom: (last ? Common.getLengthByIPhone7(16) : 0),
			backgroundColor: 'white',
			alignItems: 'center',
			shadowColor: "#000",
			shadowOffset: {
				width: 0,
				height: 1,
			},
			shadowOpacity: 0.18,
			shadowRadius: 5.00,
			elevation: 1,
		}}
		activeOpacity={0.7}
		onPress={() => {
			onPress();
		}}>
			<View style={{
				width: Common.getLengthByIPhone7(0) - Common.getLengthByIPhone7(32),
				alignItems: 'center',
				borderRadius: Common.getLengthByIPhone7(10),
				overflow: 'hidden',
			}}>
				<View style={{

				}}>
					<PhotoView
						source={BASE_URL + item.photos[0].file}
						style={{
							width: Common.getLengthByIPhone7(0) - Common.getLengthByIPhone7(32),
							height: Common.getLengthByIPhone7(200),
						}}
					/>
					<TouchableOpacity style={{
						width: Common.getLengthByIPhone7(32),
						height: Common.getLengthByIPhone7(32),
						borderRadius: Common.getLengthByIPhone7(16),
						backgroundColor: 'white',
						alignItems: 'center',
						justifyContent: 'center',
						position: 'absolute',
						right: Common.getLengthByIPhone7(16),
						top: Common.getLengthByIPhone7(16),
					}}
					onPress={() => {
						like(item.id);
					}}>
						<Image
							style={{
								resizeMode: 'contain',
								width: Common.getLengthByIPhone7(18),
								height: Common.getLengthByIPhone7(16),
								tintColor: (favorites.includes(item.id) ? null : 'gray')
							}}
							source={require('./../../assets/ic-heard.png')}
						/>
					</TouchableOpacity>
				</View>
				<Text style={{
					width: Common.getLengthByIPhone7(0) - Common.getLengthByIPhone7(52),
					color: colors.BLACK,
					fontFamily: 'Rubik-Medium',
					fontWeight: '500',
					textAlign: 'left',
					fontSize: Common.getLengthByIPhone7(16),
					lineHeight: Common.getLengthByIPhone7(18),
					letterSpacing: -0.33,
					marginBottom: Common.getLengthByIPhone7(10),
					marginTop: Common.getLengthByIPhone7(10),
				}}
				allowFontScaling={false}>
					{item.name}
				</Text>
				<Text style={{
					width: Common.getLengthByIPhone7(0) - Common.getLengthByIPhone7(52),
					color: colors.GRAY_LIGHT,
					fontFamily: 'OpenSans-Regular',
					fontWeight: 'normal',
					textAlign: 'right',
					fontSize: Common.getLengthByIPhone7(12),
					lineHeight: Common.getLengthByIPhone7(14),
					letterSpacing: -0.33,
					marginBottom: Common.getLengthByIPhone7(10),
					// marginTop: Common.getLengthByIPhone7(10),
				}}
				allowFontScaling={false}>
					~{distance} км
				</Text>
			</View>
		</TouchableOpacity>
	);
};

const mstp = (state: RootState) => ({
	coords: state.location.coords,
	favorites: state.buttons.favorites,
});

const mdtp = (dispatch: Dispatch) => ({
    like: id => dispatch.buttons.like(id),
});

export default connect(mstp, mdtp)(PointView);