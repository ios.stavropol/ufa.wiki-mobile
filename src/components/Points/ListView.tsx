import React, { useEffect } from 'react';
import {StyleSheet, FlatList, TextInput, View, RefreshControl, Text, TouchableOpacity, Image} from 'react-native';
import { useRoute } from '@react-navigation/native';
import { RootState, Dispatch } from './../../store';
import Common from '../../utilities/Common';
import PointView from '../../components/Points/PointView';
import { useHeaderHeight } from '@react-navigation/elements';
import TagsView from './TagsView';
import SearchView from './SearchView';

const ListView = ({points, onRefresh, isLoading}) => {
	const params = useRoute().params;
    const headerHeight = useHeaderHeight();
    // const dispatch = useDispatch<Dispatch>()
    // const pointsState = useSelector((state: RootState) => state.points)

	//@ts-ignore
	const data = params ? params.data : null;
	console.log('Points -> data', data);

    useEffect(() => {
        onRefresh();
    }, []);

    const renderRow = (item: object, index: number) => {
        return (<PointView
            item={item}
            index={index}
            last={points.points.length === index + 1 ? true : false}
            onPress={() => {

            }}
        />);
    } 

	return (
		<View style={{
            flex: 1,
            backgroundColor: 'transparent',
            marginBottom: -20,
            marginTop: headerHeight + 20,
        }}>
            <SearchView/>
            <TagsView/>
            <FlatList
                style={{
                    flex: 1,
                    backgroundColor: 'transparent',
                    width: Common.getLengthByIPhone7(0),
                    marginBottom: 60,
                    // marginTop: Common.getLengthByIPhone7(10),
                    zIndex: 1,
                }}
                contentContainerStyle={{
                    alignItems: 'center',
                    justifyContent: 'flex-start',
                }}
                refreshControl={
                    <RefreshControl refreshing={isLoading} onRefresh={onRefresh} />
                }
                bounces={true}
                removeClippedSubviews={false}
                scrollEventThrottle={16}
                data={points.points}
                extraData={points.points}
                keyExtractor={(item, index) => index.toString()}
                renderItem={({item, index}) => renderRow(item, index)}
            />
        </View>
	);
};

export default ListView;