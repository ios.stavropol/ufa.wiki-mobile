import React, { useEffect } from 'react';
import {View, Text, TouchableOpacity, Image} from 'react-native';
import Common from '../../utilities/Common';
import { RootState, Dispatch } from './../../store';
import { connect, useDispatch, useSelector } from 'react-redux';

const MapList = ({setMapList, maplist}) => {
	
	const dispatch = useDispatch<Dispatch>()

	// useEffect(() => {
    //     console.warn('maplist: '+maplist);
    // }, [maplist]);

	return (
		<TouchableOpacity style={{
			width: Common.getLengthByIPhone7(32),
			height: Common.getLengthByIPhone7(32),
			alignItems: 'flex-end',
			justifyContent: 'center',
		}}
		onPress={() => {
			if (maplist === 'list') {
				setMapList('map');
			} else {
				setMapList('list');
			}
		}}>
			<Image
				source={maplist === 'list' ? require('./../../assets/ic-map.png') : require('./../../assets/ic-list.png')}
				style={{
					resizeMode: 'contain',
					width: Common.getLengthByIPhone7(24),
					height: Common.getLengthByIPhone7(24),
					tintColor: 'white',
				}}
			/>
		</TouchableOpacity>
	);
};

const mstp = (state: RootState) => ({
	maplist: state.buttons.maplist,
});

const mdtp = (dispatch: Dispatch) => ({
	setMapList: payload => dispatch.buttons.setMapList(payload),
});

export default connect(mstp, mdtp)(MapList);